This project will not host source code.

The purpose of this project is to
 - manage user issues https://git.uwaterloo.ca/mfcf/jupyter/issues
 - track project progress https://git.uwaterloo.ca/mfcf/jupyter/milestones
